import multiprocessing
import collections
import glob
import numpy as np
from scipy.integrate import simps

from postprocessing.mp import *

class FluxMapCreator(Operator):
    def preprocess(self):
        # initialize internal flux map
        self._flux_map = {}

    def process(self,asc_chunk):
        # process lines in ascii chunk and extract quantities
        for line in asc_chunk:
            cols = line.split()
            if len(cols) != 0:
                current_it = int(cols[0])
                t,x,y,z = float(cols[1]),float(cols[2]),float(cols[3]),float(cols[4])
                result = 0.0
                if float(cols[7]) > 0:
                    result = float(cols[5])*float(cols[8])
                # update internal flux map
                if current_it not in self._flux_map:
                    self._flux_map[current_it] = collections.defaultdict(float)
                # duplicates get overridden here
                self._flux_map[current_it][(t,x,y,z)] = result

    def postprocess(self):
        return self._flux_map

class FluxMapCombiner(FluxMapCreator):
    def process(self,flux_map):
        # merge flux_map into internal one
        for it in flux_map:
            if it in self._flux_map:
                self._flux_map[it].update(flux_map[it])
            else:
                self._flux_map[it] = flux_map[it]

    def postprocess(self):
        # compute total flux, per iteration
        total_flux = {}
        for it in self._flux_map:
            total_flux[it] = self.computeTotalFlux(self._flux_map[it])
        return total_flux

    def computeTotalFlux(self,sphere_flux_map):
        # compute total flux of full sphere
        time = sphere_flux_map.keys()[0][0]
        total_flux = 0.0
        for flux_element in sphere_flux_map:
            total_flux += sphere_flux_map[flux_element]
        return [time,total_flux]

if __name__ == "__main__":
    # files should lie on a parallel fs (e.g. scratch) to take advantage of the mp reading
    filenames0 = glob.glob("/home/astro/papenfort/scratch/tmp/outflow/output-000*/outflow_surface_det_0_fluxdens.asc")
    filenames1 = glob.glob("/home/astro/papenfort/scratch/tmp/outflow/output-000*/outflow_surface_det_1_fluxdens.asc")
    filenames2 = glob.glob("/home/astro/papenfort/scratch/tmp/outflow/output-000*/outflow_surface_det_2_fluxdens.asc")

    # how much parser processes per "file"
    num_workers = 7

    tot_filenames = [filenames0,filenames1,filenames2]

    # initialize parallel readers and get ouput queues
    mp_readers = [ParallelChunkReader(filenames,skip_first=4,num_term_sigs=num_workers) for filenames in tot_filenames]
    reader_queues = [reader.getQueue() for reader in mp_readers]

    # initialize parallel parsers and get ouput queues
    mp_workers = [ParallelWorker(in_queue,FluxMapCreator(),num_procs=num_workers,num_term_sigs=1) for in_queue in reader_queues]
    worker_queues = [mp_worker.getQueue() for mp_worker in mp_workers]

    # initialize combine and total flux processes and get output queues
    workers = [Worker(in_queue,FluxMapCombiner()) for in_queue in worker_queues]
    workers_queues = [worker.getQueue() for worker in workers]

    # start all processes
    for reader in mp_readers:
        reader.start()
    for mp_worker in mp_workers:
        mp_worker.start()
    for worker in workers:
        worker.start()

    print "Waiting for readers to finish..."
    for reader in mp_readers:
        reader.join()
    print "Waiting for parsers to finish..."
    for worker in mp_workers:
        worker.join()

    print "Waiting for combine/total flux computation to finish..."
    for worker in workers:
        worker.join()

    print "Starting to integrate fluxes to get total unbound mass per iteration"
    for counter,queue in enumerate(workers_queues):
        total_flux = queue.get()
        time = []
        flux = []
        iters = sorted(total_flux.keys())
        for it in iters:
            time.append(total_flux[it][0])
            flux.append(total_flux[it][1])
        total_mass = []
        total_mass_sum = []
        for i in range(1,len(time)+1):
            total_mass.append(simps(flux[:i],time[:i]))

            sum = 0
            dt = time[1]-time[0]
            for f in flux:
                sum += f*dt
            total_mass_sum.append(sum)
        print len(total_mass),len(total_mass_sum),len(time)
        cols = np.column_stack((time,total_mass,total_mass_sum))
        np.savetxt("tot_mass_{0}.asc".format(counter),cols,delimiter=" ")

    print "done."
