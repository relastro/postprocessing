import multiprocessing as _multiprocessing
import itertools as _itertools

class Operator(object):
    """ Operator base class, which gets passed to the Worker

        This class skips any processing and throws inputs away
    """

    def preprocess(self):
        """ Overwrite this, if preprocessing is necessary """
        pass

    def postprocess(self):
        """ Overwrite this, if postprocessing is necessary """
        pass

    def process(self,element):
        """ Overwrite this, if you want to process elements from a queue """
        pass

class Worker(_multiprocessing.Process):
    """ A seperate process that passes queue elements to a Operator implementation """
    def __init__(self,in_queue,operator=Operator(),out_queue=None,max_q_len=0,term_signal=None,num_term_sigs=0):
        """ Initialize process
            Note: args get copied to child process once it is startet

            Args:
                in_queue:       input queue from which the elements are processed
                operator:      target process object to call with args from in_queue, defaults to abstract Operator class
                out_queue:      queue to pass the results (defaults to None -> internal Queue is created)
                max_q_len:      maximal length of out_queue (defaults to 0, no limit)
                                Note: out_queue blocks process as long as it is full
                term_signal:    which termination signal on in_queue to expect / on out_queue to send (defaults to None)
                num_term_sigs:  number of termination signals to send to queue (defaults to 0, no termination signal)
        """
        # init base class
        super(Worker, self).__init__()
        # define everything what's needed
        self._in_queue = in_queue
        # if no output queue is given, create own one
        if out_queue == None:
            self._manager = _multiprocessing.Manager()
            if max_q_len == 0:
                self._out_queue = self._manager.Queue()
            else:
                self._out_queue = self._manager.Queue(max_q_len)
        else:
            self._out_queue = out_queue
        # define termination signals
        self._term_signal = term_signal
        self._num_term_sigs = num_term_sigs
        # define calls
        self._operator = operator
    def run(self):
        """ Actual processing routine

            Do NOT call this, if you want to read in a seperate process!
            Use start() and join(), inherited from multiprocessing.Process
        """
        self._operator.preprocess()
        b = True
        while b:
            # get input from queue
            input = self._in_queue.get()
            if input != self._term_signal:
                # Get result from process, put on out queue if not None
                result = self._operator.process(input)
                if result != None:
                    self._out_queue.put(result)
            else:
                # terminate on term_signal
                b = False
        # call postprocess, put on queue if not None
        postprocessed = self._operator.postprocess()
        if postprocessed != None:
            self._out_queue.put(postprocessed)
        # send termination signals to queue, if any
        for _ in range(self._num_term_sigs):
            self._out_queue.put(self._term_signal)
    def getQueue(self):
        """ Returns the output queue.

            Each element is a result from external function/postprocess call
        """
        return self._out_queue

class ParallelWorker(_multiprocessing.Process):
    """ A seperate process that passes a queue and Operator implementations to multiple Workers """
    def __init__(self,in_queue,operator=Operator(),out_queue=None,max_q_len=0,term_signal=None,num_term_sigs=0,num_procs=4):
        """ Initialize process
            Note: args get copied to child process once it is startet

            Args:
                in_queue:       input queue from which the elements are processed
                operator:      target process object to call with args from in_queue, defaults to abstract Operator class
                out_queue:      queue to pass the results (defaults to None -> internal Queue is created)
                max_q_len:      maximal length of out_queue (defaults to 0, no limit)
                                Note: out_queue blocks process as long as it is full
                term_signal:    which termination signal on in_queue to expect / out_queue to send (defaults to None)
                num_term_sigs:  number of termination signals to send to queue (defaults to 0, no termination signal)
                num_procs:      number of parallel processes (defaults to 4)
        """
        # init base class
        super(ParallelWorker, self).__init__()
        # define everything what's needed
        self._in_queue = in_queue
        self._num_procs = num_procs
        # if no output queue is given, create own one
        if out_queue == None:
            self._manager = _multiprocessing.Manager()
            if max_q_len == 0:
                self._out_queue = self._manager.Queue()
            else:
                self._out_queue = self._manager.Queue(max_q_len)
        else:
            self._out_queue = out_queue
        # define termination signal
        self._term_signal = term_signal
        self._num_term_sigs = num_term_sigs
        # calls
        self._operator = operator
    def run(self):
        """ Actual managing routine, running num_proc Workers in parallel

            Do NOT call this, if you want to read in a seperate process!
            Use start() and join(), inherited from multiprocessing.Process
        """
        # Create num_proc Worker instances
        mp_workers = [Worker(self._in_queue,self._operator,self._out_queue,term_signal=self._term_signal) for _ in range(self._num_procs)]
        for worker in mp_workers:
            worker.start()
        for worker in mp_workers:
            worker.join()
        # send termination signals to queue, if any
        for _ in range(self._num_term_sigs):
            self._out_queue.put(self._term_signal)
    def getQueue(self):
        """ Returns the output queue.

            Each element is a result from external function/postprocess call
        """
        return self._out_queue

class ChunkReader(_multiprocessing.Process):
    """ A seperate process that reads in given file in chunks of given size """
    def __init__(self,filename,out_queue=None,chunksize=2048,max_q_len=0,skip_first=0,term_signal=None,num_term_sigs=0):
        """ Initialize process
            Note: args get copied once process is startet to child process

            Args:
                filename:       file to read in
                out_queue:      queue to pass the chunks (defaults to None, internal Queue is created)
                chunksize:      lines to read at once (defaults to 2048)
                max_q_len:      maximal length of out_queue (defaults to 0, no limit)
                                Note: out_queue blocks process as long as it is full
                skip_first:     number of lines to skip at the beginning of the file (defaults to 0)
                term_signal:    termination signal pushed to queue (defaults to None)
                num_term_sigs:  number of termination signals to send to queue (defaults to 0, no termination signal)
        """
        # init base class
        super(ChunkReader, self).__init__()
        # define everything what's needed
        self._filename = filename
        # if no output queue is given, create own one
        if out_queue == None:
            self._manager = _multiprocessing.Manager()
            if max_q_len == 0:
                self._out_queue = self._manager.Queue()
            else:
                self._out_queue = self._manager.Queue(max_q_len)
        else:
            self._out_queue = out_queue
        self._chunksize = chunksize
        self._skip_first = skip_first
        self._term_signal = term_signal
        self._num_term_sigs = num_term_sigs
    def run(self):
        """ Actual reading routine

            Do NOT call this, if you want to read in a seperate process!
            Use start() and join(), inherited from multiprocessing.Process
        """
        # open file
        with open(self._filename,"r") as f:
            # skip lines, if necessary
            for _ in range(self._skip_first):
                next(f)
            # get the first chunk
            line_buffer = list(_itertools.islice(f,self._chunksize))
            # redo it until nothing is left
            while len(line_buffer) > 0:
                # put chunk to queue, from where it can be processed by another process
                self._out_queue.put(line_buffer)
                line_buffer = list(_itertools.islice(f,self._chunksize))
        # send termination signals to queue, if any
        for _ in range(self._num_term_sigs):
            self._out_queue.put(self._term_signal)
    def getQueue(self):
        """ Returns the output queue.

            Each element is a list of lines of length chunksize.
        """
        return self._out_queue


class ParallelChunkReader(_multiprocessing.Process):
    """ A seperate process that manages multiple ChunkReader instances which read files in parallel """
    def __init__(self,filenames,out_queue=None,chunksize=2048,max_q_len=0,skip_first=0,num_procs=0,term_signal=None,num_term_sigs=0):
        """ Initialize ChunkReader manager

            Args:
                filenames:      list of files, read by multiple ChunkReaders in parallel
                out_queue:      queue to pass the chunks (defaults to None -> internal Queue is created)
                chunksize:      lines to read at once (defaults to 2048)
                max_q_len:      maximal length of out_queue (defaults to 0, no limit)
                                Note: out_queue blocks process as long as it is full
                skip_first:     number of lines to skip at the beginning of the files (defaults to 0)
                num_procs:      number of parallel processes (defaults to number of files)
                term_signal:    termination signal pushed to queue (defaults to None)
                num_term_sigs:  number of termination signals to send to queue (defaults to 0, no termination signal)
        """
        # init base class
        super(ParallelChunkReader, self).__init__()
        # get num_procs or set to number of files
        if num_procs == 0:
            self._num_procs = len(filenames)
        else:
            self._num_procs = num_procs
        if out_queue == None:
            # create output Queue instance, shared by processes
            self._manager = _multiprocessing.Manager()
            if max_q_len == 0:
                self._out_queue = self._manager.Queue()
            else:
                self._out_queue = self._manager.Queue(max_q_len)
        else:
            self._out_queue = out_queue
        # save parameters for process forked through start()
        self._filenames = filenames
        self._chunksize = chunksize
        self._skip_first = skip_first
        self._term_signal = term_signal
        self._num_term_sigs = num_term_sigs
    def run(self):
        """ Actual managing routine, running num_proc readers in parallel

            Do NOT call this, if you want to let it manage in a seperate process!
            Use start() and join(), inherited from mulitprocessing.Process
        """
        # Create ChunkReader instances, one per file
        mp_readers = [ChunkReader(file,out_queue=self._out_queue,chunksize=self._chunksize,skip_first=self._skip_first,term_signal=self._term_signal) for file in self._filenames]
        # list of running readers
        running = []
        for reader in mp_readers:
            # check process bound and try to join processes
            while len(running) >= self._num_procs:
                for r_reader in running:
                    r_reader.join(0.1)
                # kick terminated readers
                running[:] = [r_reader for r_reader in running if r_reader.is_alive()]
            # start next reader
            reader.start()
            running.append(reader)
        # lock until every reader has finished, enables use of inherited join()
        for reader in mp_readers:
            reader.join()
        # send termination signals to queue, if any
        for _ in range(self._num_term_sigs):
            self._out_queue.put(self._term_signal)
    def getQueue(self):
        """ Returns the internal output queue.

            Each element in the queue is a list of lines of length chunksize
        """
        return self._out_queue
